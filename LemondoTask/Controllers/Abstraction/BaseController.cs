﻿using LemondoTask.Requests.Filters.Abstraction;
using LemondoTask.Responses.Wrappers;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using LemondoTask.Responses.Queries.Abstraction;

namespace LemondoTask.Controllers.Abstraction
{
    [ApiController]
    public class BaseController : ControllerBase
    {
        #region CUSTOM RESPONSE ACTIONS

        protected ActionResult Success(object data)
            => Success(data, default);

        protected ActionResult Success(object data, string message)
            => Ok(new SuccessResponse(data, message));

        protected ActionResult BadRequest(string message)
            => BadRequest(new ErrorResponse(message));

        protected ActionResult NotFound(string message)
            => NotFound(new ErrorResponse(message, StatusCodes.Status404NotFound));

        protected ActionResult CreatedAtAction<TQuery>(string actionName, TQuery created)
            where TQuery : BaseQuery
            => CreatedAtAction(actionName, new { id = created?.Id }, new SuccessResponse(created));

        protected ActionResult PagedResponse<TData>(IEnumerable<TData> responseData, BasePagedFilter validFilter, int totalRecords)
        {
            var totalPages = totalRecords / (double)validFilter.PageSize;

            int roundedTotalPages = Convert.ToInt32(Math.Ceiling(totalPages));

            var response = new PagedResponse<IEnumerable<TData>>(responseData, validFilter.Page, validFilter.PageSize,
                roundedTotalPages, totalRecords);

            return Ok(response);
        }

        #endregion

    }
}
