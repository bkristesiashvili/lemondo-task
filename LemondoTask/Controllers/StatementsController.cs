﻿using LemondoTask.Controllers.Abstraction;
using LemondoTask.Requests.Filters;
using LemondoTask.Services.Abstractions;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using Swashbuckle.AspNetCore.Annotations;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LemondoTask.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class StatementsController : BaseController
    {
        #region PRIVATE PROPERTIES

        public IStatementsService Service { get; }

        #endregion

        #region CTOR

        public StatementsController(IStatementsService service)
            => Service = service;

        #endregion

        #region ACTIONS

        [HttpGet]
        public ActionResult Get([FromQuery] StatementPagedFilter filter)
        {
            var statements = Service.GetAllStatements(filter);

            var totalRecords = statements.Count();

            statements = statements
                .Skip((filter.Page - 1) * filter.PageSize)
                .Take(filter.PageSize);

            return PagedResponse(statements, filter, totalRecords);
        }

        [HttpGet("{id}")]
        public ActionResult Get(Guid id)
        {
            var statement = Service.GetStatementById(id);

            return statement != null
                ? Success(statement)
                : NotFound("ჩანაწერი ვერ მოიძებნა!");
        }

        #endregion
    }
}
