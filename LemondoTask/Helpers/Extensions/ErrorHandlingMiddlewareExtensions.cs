﻿using LemondoTask.Middlewares;

using Microsoft.AspNetCore.Builder;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LemondoTask.Helpers.Extensions
{
    public static class ErrorHandlingMiddlewareExtensions
    {
        #region EXTENSION METHOD

        public static IApplicationBuilder UseErrorHandling(this IApplicationBuilder builder)
            => builder.UseMiddleware<ErrorHandlingMiddleware>();

        #endregion
    }
}
