﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace LemondoTask.Helpers.Extensions
{
    public static class ObjectToJsonConverterExtension
    {
        #region EXTENSION METHODS

        public static string AsJSON(this object @this)
            => JsonSerializer.Serialize(@this);

        #endregion
    }
}
