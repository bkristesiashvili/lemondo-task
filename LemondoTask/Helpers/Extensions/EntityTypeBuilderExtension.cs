﻿using LemondoTask.Domain.Entities.Abstraction;
using LemondoTask.Domain.Seeder.Abstraction;

using Microsoft.EntityFrameworkCore.Metadata.Builders;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LemondoTask.Helpers.Extensions
{
    public static class EntityTypeBuilderExtension
    {
        #region EXTENSION METHOD

        public static void SeedData<TEntity>(this EntityTypeBuilder builder, IDataSeeder<TEntity> seeder)
            where TEntity: BaseEntity, new()
        {
            if (builder == null)
                throw new ArgumentNullException(nameof(builder));

            if (seeder == null)
                throw new ArgumentNullException(nameof(seeder));

            var data = seeder.ExecuteSeeder();

            if (!data.Any()) return;

            builder.HasData(data);
        }

        #endregion
    }
}
