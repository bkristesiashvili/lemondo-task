﻿using LemondoTask.Domain;
using LemondoTask.Domain.UnitOfWorks;
using LemondoTask.Domain.UnitOfWorks.Abstraction;
using LemondoTask.Services;
using LemondoTask.Services.Abstractions;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LemondoTask.Helpers.Extensions
{
    public static class ServiceCollectionExtensions
    {
        #region EXTENSION METHODS

        public static void AddCustomDependencies(this IServiceCollection services, IConfiguration Configuration)
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));

            if (Configuration == null)
                throw new ArgumentNullException(nameof(Configuration));

            services.AddDbContext<AppDbContext>(options =>
            {
                options.UseNpgsql(Configuration.GetConnectionString("lemondo_task_db"));
            });

            services.AddScoped<IUnitOfRepositories, UnitOfRepositories>();
            services.AddScoped<IStatementsService, StatementsService>();
        }

        #endregion
    }
}
