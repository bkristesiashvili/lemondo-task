﻿namespace LemondoTask.Helpers.Types
{
    public readonly struct OrderingTypes
    {
        #region CONSTANTS

        public const string ASCENDING   = "asc";
        public const string DESCENDING  = "desc";

        #endregion
    }
}
