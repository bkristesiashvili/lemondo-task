﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LemondoTask.Requests.Filters.Abstraction
{
    public interface IFilter
    {
        #region INTERFCAE PROPERTIES

        string Search { get; set; }

        string OrderBy { get; set; }

        string Ordering { get; set; }

        #endregion
    }
}
