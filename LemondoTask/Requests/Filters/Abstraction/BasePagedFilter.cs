﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using LemondoTask.Helpers.Types;

namespace LemondoTask.Requests.Filters.Abstraction
{
    public abstract class BasePagedFilter : IFilter
    {
        #region PRIVATE FIELDS

        #region CONSTANTS

        private const int MAX_PAGESIZE = 150;
        private const int MIN_PAGESIZE = 10;
        private const int MIN_PAGE_NUMBER = 1;
        private const string JS_UNDEFINED = "undefined";
        private string _ordering = OrderingTypes.ASCENDING;

        #endregion

        private int _pageSize = MIN_PAGESIZE;

        private int _pageNumber = MIN_PAGE_NUMBER;

        private string _searchBy = string.Empty;

        private string _orderBy = "id";

        #endregion

        #region PUBLIC PROPERTIES

        public int Page
        {
            get => _pageNumber;
            set
            {
                _pageNumber = value < MIN_PAGE_NUMBER ? MIN_PAGE_NUMBER : value;
            }
        }

        public int PageSize
        {
            get => _pageSize;
            set
            {
                _pageSize = value < MIN_PAGESIZE || value > MAX_PAGESIZE ? MIN_PAGESIZE : value;
            }
        }

        public string Search
        {
            get => _searchBy;
            set
            {
                _searchBy = string.IsNullOrEmpty(value) ||
                            string.IsNullOrWhiteSpace(value) ||
                            value.ToLower().Equals(JS_UNDEFINED) ? string.Empty : value;
            }
        }

        public string OrderBy
        {
            get => _orderBy;
            set => _orderBy = value;
        }

        public string Ordering
        {
            get => _ordering;
            set
            {
                _ordering = value.ToLower().Equals(OrderingTypes.DESCENDING) ?
                    OrderingTypes.DESCENDING : OrderingTypes.ASCENDING;
            }
        }

        #endregion

        #region CONSTRUCTOR

        public BasePagedFilter()
        {
            _pageNumber = 1;
            _pageSize = 10;
        }

        #endregion
    }
}
