﻿using LemondoTask.Helpers.Types;
using LemondoTask.Requests.Filters.Abstraction;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LemondoTask.Requests.Filters
{
    public sealed class StatementPagedFilter : BasePagedFilter
    {
        #region CTOR

        public StatementPagedFilter()
        {
            OrderBy = "title";
            Ordering = OrderingTypes.ASCENDING;
        }

        #endregion
    }
}
