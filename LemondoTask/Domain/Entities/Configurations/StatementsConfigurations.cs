﻿using LemondoTask.Domain.Entities.Configurations.Abstraction;
using LemondoTask.Domain.Seeder;
using LemondoTask.Helpers.Extensions;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LemondoTask.Domain.Entities.Configurations
{
    public sealed class StatementsConfigurations : BaseConfiguration<Statement>
    {
        #region OVERRIDED CONFIGURE METHOD

        public override void Configure(EntityTypeBuilder<Statement> builder)
        {
            builder.ToTable("statements");

            builder.SeedData(new StatementSeeder());

            base.Configure(builder);
        }

        #endregion
    }
}
