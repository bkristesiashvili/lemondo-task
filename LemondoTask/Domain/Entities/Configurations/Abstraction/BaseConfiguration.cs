﻿using LemondoTask.Domain.Entities.Abstraction;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LemondoTask.Domain.Entities.Configurations.Abstraction
{
    public abstract class BaseConfiguration<TEntity> : IEntityTypeConfiguration<TEntity>
        where TEntity : BaseEntity, new()
    {
        #region IMPLEMENTATION CONFIGURE METHOD

        public virtual void Configure(EntityTypeBuilder<TEntity> builder)
        {
            builder.HasKey(entity => entity.Id);

            builder.HasIndex(entity => entity.Id).IsUnique();

            builder.Property(entity => entity.CreatedAt).HasDefaultValueSql("NOW()")
                .ValueGeneratedOnAdd();

            builder.Property(entity => entity.UpdatedAt).HasDefaultValueSql("NOW()")
                .ValueGeneratedOnAdd();
        }

        #endregion
    }
}
