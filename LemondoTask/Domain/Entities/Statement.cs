﻿using LemondoTask.Domain.Entities.Abstraction;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LemondoTask.Domain.Entities
{
    public class Statement : BaseEntity
    {
        #region PUBLIC PROPERTIES

        [Required, DataType(DataType.Text)]
        [Column("statement_title")]
        public string Title { get; set; }

        [Required ,DataType(DataType.Text)]
        [Column("statement_description")]
        public string Description { get; set; }

        [Required, DataType(DataType.PhoneNumber)]
        [Column("statement_phone")]
        public string Phone { get; set; }

        [Column("statement_image")]
        public string Image { get; set; }

        #endregion
    }
}
