﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LemondoTask.Domain.Entities.Abstraction
{
    public abstract class BaseEntity
    {
        #region PUBLIC PROPERTIES

        [Key]
        [Column("uuid")]
        public Guid Id { get; set; }

        [Column("created_at")]
        public DateTime CreatedAt { get; set; }

        [Column("updated_at")]
        public DateTime UpdatedAt { get; set; }

        #endregion
    }
}
