﻿using LemondoTask.Domain.Entities;
using LemondoTask.Domain.Seeder.Abstraction;

using System;
using System.Collections.Generic;

namespace LemondoTask.Domain.Seeder
{
    public sealed class StatementSeeder : IDataSeeder<Statement>
    {
        #region IMPLEMENTATION EXECUTE SEEDER METHOD

        public IReadOnlyList<Statement> ExecuteSeeder() => new List<Statement>
        {
            new Statement{ Id= Guid.NewGuid(), Title ="ტესტ განცხადება #1", Description="ტესტ განცხადების აღწერა", Phone="0322-00-00-00" },
            new Statement{ Id= Guid.NewGuid(), Title ="ტესტ განცხადება #2", Description="ტესტ განცხადების აღწერა", Phone="0322-00-00-00" },
            new Statement{ Id= Guid.NewGuid(), Title ="ტესტ განცხადება #3", Description="ტესტ განცხადების აღწერა", Phone="0322-00-00-00" },
            new Statement{ Id= Guid.NewGuid(), Title ="ტესტ განცხადება #4", Description="ტესტ განცხადების აღწერა", Phone="0322-00-00-00" },
            new Statement{ Id= Guid.NewGuid(), Title ="ტესტ განცხადება #5", Description="ტესტ განცხადების აღწერა", Phone="0322-00-00-00" },
            new Statement{ Id= Guid.NewGuid(), Title ="ტესტ განცხადება #6", Description="ტესტ განცხადების აღწერა", Phone="0322-00-00-00" },
            new Statement{ Id= Guid.NewGuid(), Title ="ტესტ განცხადება #7", Description="ტესტ განცხადების აღწერა", Phone="0322-00-00-00" },
            new Statement{ Id= Guid.NewGuid(), Title ="ტესტ განცხადება #8", Description="ტესტ განცხადების აღწერა", Phone="0322-00-00-00" },
        }.AsReadOnly();

        #endregion
    }
}
