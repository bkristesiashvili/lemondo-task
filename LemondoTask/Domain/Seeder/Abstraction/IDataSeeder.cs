﻿using LemondoTask.Domain.Entities.Abstraction;

using System.Collections.Generic;

namespace LemondoTask.Domain.Seeder.Abstraction
{
    public interface IDataSeeder<TEntity>
        where TEntity : BaseEntity, new()
    {
        #region INTERFACE METHOD

        IReadOnlyList<TEntity> ExecuteSeeder();

        #endregion
    }
}
