﻿using LemondoTask.Domain.Entities.Abstraction;
using LemondoTask.Requests.Filters.Abstraction;
using LemondoTask.Helpers.Extensions;

using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LemondoTask.Domain.Repositories.Abstraction
{
    public abstract class BaseRepository<TEntity> : IDisposable
        where TEntity: BaseEntity, new()
    {
        #region PRIVATE FIELDS

        private bool disposed;

        #endregion

        #region CTOR

        public BaseRepository(AppDbContext context)
        {
            DbContext = context;
            Entity = DbContext.Set<TEntity>();
        }

        #endregion

        #region PUBLIC PROPERTIES

        public AppDbContext DbContext { get; }

        public DbSet<TEntity> Entity { get; }

        #endregion

        #region PUBLIC METHODS

        public virtual IQueryable<TEntity> SelectAll(IFilter filter = null)
        {
            EnsureDependencies();
            return SortBy(Entity, filter);
        }

        public virtual TEntity SelectById(Guid id)
        {
            EnsureDependencies();
            return Entity.SingleOrDefault(entity => entity.Id.Equals(id));
        }

        public virtual void Insert(TEntity newEntity)
        {
            EnsureDependencies();

            if (newEntity == null)
                throw new ArgumentNullException(nameof(newEntity));

            Entity.Add(newEntity);
        }

        public virtual void Update(TEntity updatedEntity)
        {
            EnsureDependencies();

            if (updatedEntity == null)
                throw new ArgumentNullException(nameof(updatedEntity));

            DbContext.Entry<TEntity>(updatedEntity).State = EntityState.Modified;
        }

        public virtual void Delete(TEntity deleteEntity)
        {
            EnsureDependencies();

            if (deleteEntity == null)
                throw new ArgumentNullException(nameof(deleteEntity));

            Entity.Remove(deleteEntity);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        #endregion

        #region PROTECTED METHODS

        protected bool CheckSearchFilter(IFilter filter)
        {
            var filtered = filter != null &&
                (!string.IsNullOrEmpty(filter?.Search) ||
                 !string.IsNullOrWhiteSpace(filter?.Search));

            return filtered;
        }

        protected IQueryable<TEntity> SortBy(IQueryable<TEntity> entities,
            IFilter filter = null)
        {
            if (!entities.Any()) return entities;

            if (filter == null) return entities;

            if (string.IsNullOrEmpty(filter.OrderBy) || string.IsNullOrWhiteSpace(filter.OrderBy))
                return entities;

            return entities.OrderBy(filter);
        }

        #endregion

        #region PRIVATE METHODS

        private void EnsureDependencies()
        {
            if (DbContext == null)
                throw new ArgumentNullException(nameof(DbContext));
            if (Entity == null)
                throw new ArgumentNullException(nameof(Entity));
        }

        private void Dispose(bool disposing)
        {
            if (disposed) return;
            if (disposing)
            {
                DbContext?.Dispose();
                GC.SuppressFinalize(this);
            }

            disposed = true;
        }

        #endregion
    }
}
