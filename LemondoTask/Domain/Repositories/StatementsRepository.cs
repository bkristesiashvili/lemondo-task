﻿using LemondoTask.Domain.Entities;
using LemondoTask.Domain.Repositories.Abstraction;
using LemondoTask.Requests.Filters.Abstraction;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LemondoTask.Domain.Repositories
{
    public sealed class StatementsRepository : BaseRepository<Statement>
    {
        #region CTOR

        public StatementsRepository(AppDbContext context)
            : base(context) { }

        #endregion

        #region OVERRIDED METHODS

        public override IQueryable<Statement> SelectAll(IFilter filter = null)
        {
            var statements = base.SelectAll(filter);

            return CheckSearchFilter(filter)
                ? statements.Where(entity => entity.Title.StartsWith(filter.Search) ||
                                             entity.Description.StartsWith(filter.Search) ||
                                             entity.Phone.StartsWith(filter.Search))
                : statements;
        }

        #endregion
    }
}
