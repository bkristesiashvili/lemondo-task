﻿using LemondoTask.Domain.Entities;
using LemondoTask.Domain.Repositories;
using LemondoTask.Domain.Repositories.Abstraction;
using LemondoTask.Domain.UnitOfWorks.Abstraction;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LemondoTask.Domain.UnitOfWorks
{
    public sealed class UnitOfRepositories : IUnitOfRepositories
    {
        #region PRIVATE FIELDS

        private BaseRepository<Statement> _statementRepo;
        private readonly AppDbContext _dbContext;

        #endregion

        #region CTOR

        public UnitOfRepositories(AppDbContext context)
            => _dbContext = context;

        #endregion

        #region IMPLEMENTED REPOSITORIES PROPERTIES

        public BaseRepository<Statement> StatementsRepo
        {
            get
            {
                if (_statementRepo == null)
                    _statementRepo = new StatementsRepository(_dbContext);
                return _statementRepo;
            }
        }

        #endregion
    }
}
