﻿using LemondoTask.Domain.Entities;
using LemondoTask.Domain.Repositories.Abstraction;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LemondoTask.Domain.UnitOfWorks.Abstraction
{
    public interface IUnitOfRepositories
    {
        #region REPOSITORIES PROPERTIES

        BaseRepository<Statement> StatementsRepo { get; }

        #endregion
    }
}
