﻿using LemondoTask.Domain.Entities;
using LemondoTask.Domain.Entities.Abstraction;
using LemondoTask.Domain.Entities.Configurations;

using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LemondoTask.Domain
{
    public sealed class AppDbContext : DbContext
    {
        #region CTOR

        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options) { }

        #endregion

        #region TABLE ENTITY PROPERTIES

        public DbSet<Statement> Statements{ get; set; }

        #endregion

        #region OVERRIDED METHODS

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new StatementsConfigurations());

            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            var entries = ChangeTracker.Entries()
                .Where(e => e.Entity is BaseEntity && e.State == EntityState.Modified || e.State == EntityState.Added);

            foreach (var entry in entries)
            {
                if (entry.State == EntityState.Modified)
                {
                    ((BaseEntity)entry.Entity).UpdatedAt = DateTime.Now;
                    ((BaseEntity)entry.Entity).CreatedAt = entry.OriginalValues.GetValue<DateTime>("CreatedAt");
                }
                else if (entry.State == EntityState.Added)
                {
                    ((BaseEntity)entry.Entity).UpdatedAt = DateTime.Now;
                    ((BaseEntity)entry.Entity).CreatedAt = DateTime.Now;
                }
            }

            return base.SaveChanges();
        }

        #endregion
    }
}
