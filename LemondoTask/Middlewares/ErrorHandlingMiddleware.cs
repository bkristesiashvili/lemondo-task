﻿using LemondoTask.Helpers.Extensions;
using LemondoTask.Responses.Wrappers;

using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace LemondoTask.Middlewares
{
    public sealed class ErrorHandlingMiddleware
    {
        #region PRIVATE FIELDS

        private readonly RequestDelegate requestDelegate;

        private readonly ILogger Logger;

        #endregion

        #region CONSTRUCTOR

        public ErrorHandlingMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
        {
            requestDelegate = next;

            Logger = loggerFactory.CreateLogger("Request");
        }

        #endregion

        #region PUBLIC METHODS

        public async Task Invoke(HttpContext httpContext)
        {
            try
            {
                await requestDelegate(httpContext);
            }
            catch (Exception e)
            {
                Logger.LogError($"Something went wrong: {e.Message}");
                await HandleGlobalExceptionAsync(httpContext, e);
            }
        }

        #endregion

        #region PRIVATE METHODS

        private static async Task HandleGlobalExceptionAsync(HttpContext context,
            Exception exception)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            var erro = new ErrorResponse(exception.Message, context.Response.StatusCode);

            await context.Response.WriteAsync(erro.AsJSON());
        }

        #endregion
    }
}
