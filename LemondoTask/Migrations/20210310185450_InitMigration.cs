﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LemondoTask.Migrations
{
    public partial class InitMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "statements",
                columns: table => new
                {
                    uuid = table.Column<Guid>(type: "uuid", nullable: false),
                    statement_title = table.Column<string>(type: "text", nullable: false),
                    statement_description = table.Column<string>(type: "text", nullable: false),
                    statement_phone = table.Column<string>(type: "text", nullable: false),
                    statement_image = table.Column<string>(type: "text", nullable: true),
                    created_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: false, defaultValueSql: "NOW()"),
                    updated_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: false, defaultValueSql: "NOW()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_statements", x => x.uuid);
                });

            migrationBuilder.InsertData(
                table: "statements",
                columns: new[] { "uuid", "statement_description", "statement_image", "statement_phone", "statement_title" },
                values: new object[,]
                {
                    { new Guid("2aef9804-9472-4c20-98c0-fbd7b7d8065e"), "ტესტ განცხადების აღწერა", null, "0322-00-00-00", "ტესტ განცხადება #1" },
                    { new Guid("0ccc1e45-4424-4d91-85b6-9e052d01bbee"), "ტესტ განცხადების აღწერა", null, "0322-00-00-00", "ტესტ განცხადება #2" },
                    { new Guid("f8df9fb7-7020-4889-bf71-707ac817f4b3"), "ტესტ განცხადების აღწერა", null, "0322-00-00-00", "ტესტ განცხადება #3" },
                    { new Guid("b980629b-4a9e-47da-96a9-58a1b3dac930"), "ტესტ განცხადების აღწერა", null, "0322-00-00-00", "ტესტ განცხადება #4" },
                    { new Guid("6ac4f1fd-ad10-4f99-a52c-cc1409859e76"), "ტესტ განცხადების აღწერა", null, "0322-00-00-00", "ტესტ განცხადება #5" },
                    { new Guid("9bdcfc4d-1c6a-4657-92db-fa87146a7782"), "ტესტ განცხადების აღწერა", null, "0322-00-00-00", "ტესტ განცხადება #6" },
                    { new Guid("a0c7717c-7c04-4832-afac-250c99d8c616"), "ტესტ განცხადების აღწერა", null, "0322-00-00-00", "ტესტ განცხადება #7" },
                    { new Guid("dd313cd2-a75a-4b0b-99e6-9c81384c6d85"), "ტესტ განცხადების აღწერა", null, "0322-00-00-00", "ტესტ განცხადება #8" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_statements_uuid",
                table: "statements",
                column: "uuid",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "statements");
        }
    }
}
