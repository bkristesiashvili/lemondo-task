﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

using LemondoTask.Responses.Wrappers.Abstraction;

namespace LemondoTask.Responses.Wrappers
{
    public sealed class SuccessResponse : BaseResponse<object>
    {
        #region CONSTRUCTOR

        public SuccessResponse(object data, string message = default, 
            int statusCode = (int)HttpStatusCode.OK)
        {
            Data = data;
            Success = true;
            Message = default;
            StatusCode = statusCode;
        }

        #endregion
    }
}
