﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

using LemondoTask.Responses.Wrappers.Abstraction;

namespace LemondoTask.Responses.Wrappers
{
    public sealed class PagedResponse<TData> : BaseResponse<TData>
    {
        #region CONSTRUCTOR

        public PagedResponse(TData data, int page, int pageSize, int totaalPages = 0, int totalRecords = 0)
        {
            Data = data;
            Page = page;
            PageSize = pageSize;
            TotalPages = totaalPages;
            TotalRecords = totalRecords;
            Success = true;
            Message = default;
            StatusCode = (int)HttpStatusCode.OK;
        }

        #endregion

        #region PAGINATION PROPERTIES

        public int Page { get; }

        public int PageSize { get; }

        public int TotalPages { get; }

        public int TotalRecords { get; }

        #endregion
    }
}
