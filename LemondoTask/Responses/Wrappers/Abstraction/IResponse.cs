﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LemondoTask.Responses.Wrappers.Abstraction
{
    public interface IResponse<T>
    {
        #region INTERFACE PROPERTIES

        T Data { get; }

        bool Success { get; }

        string Message { get; }

        int StatusCode { get; }

        #endregion
    }
}
