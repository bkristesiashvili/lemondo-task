﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LemondoTask.Responses.Wrappers.Abstraction
{
    public abstract class BaseResponse<T> : IResponse<T>
    {
        #region CONSTRUCTORS

        public BaseResponse(T data, string message = default)
        {
            Data = data;
            Success = data == null ? false : true;
            Message = message;
        }

        public BaseResponse(string errorMessage) : this(default, errorMessage) { }

        public BaseResponse() : this(default) { }

        #endregion

        #region PUBLIC IMPLEMENTED PROPERTIES

        public T Data { get; protected set; }

        public bool Success { get; protected set; }

        public string Message { get; protected set; }

        public int StatusCode { get; protected set; }

        #endregion
    }
}
