﻿using LemondoTask.Responses.Queries.Abstraction;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LemondoTask.Responses.Queries
{
    public sealed class StatementsListQuery : BaseQuery
    {
        #region PUBLIC PROPERTIES

        public string Title { get; set; }

        public string Base64Image { get; set; }

        #endregion
    }
}
