﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LemondoTask.Responses.Queries.Abstraction
{
    public abstract class BaseQuery
    {
        #region PUBLIC PROPERTIES

        public Guid Id { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }

        #endregion
    }
}
