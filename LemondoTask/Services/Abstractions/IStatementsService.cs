﻿using LemondoTask.Requests.Filters.Abstraction;
using LemondoTask.Responses.Queries;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LemondoTask.Services.Abstractions
{
    public interface IStatementsService
    {
        #region INTERFACE METHODS

        IEnumerable<StatementsListQuery> GetAllStatements(IFilter filter = null);

        StatementDetailQuery GetStatementById(Guid id);

        #endregion
    }
}
