﻿using LemondoTask.Domain.UnitOfWorks.Abstraction;
using LemondoTask.Requests.Filters.Abstraction;
using LemondoTask.Responses.Queries;
using LemondoTask.Services.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LemondoTask.Services
{
    public sealed class StatementsService : IStatementsService
    {
        #region PRIVATE PROPERTIES

        private IUnitOfRepositories Repositories { get; set; }

        #endregion

        #region CTOR

        public StatementsService(IUnitOfRepositories works)
            => Repositories = works;

        #endregion

        #region IMPLEMENTED METHODS

        public IEnumerable<StatementsListQuery> GetAllStatements(IFilter filter = null)
        {
            var statements = Repositories.StatementsRepo.SelectAll(filter);

            var statementsResult = from s in statements
                                  select new StatementsListQuery
                                  {
                                      Id = s.Id,
                                      Title = s.Title,
                                      Base64Image = s.Image,
                                      CreatedAt = s.CreatedAt,
                                      UpdatedAt = s.UpdatedAt
                                  };

            return statementsResult.ToList();
        }

        public StatementDetailQuery GetStatementById(Guid id)
        {
            var statement = Repositories.StatementsRepo.SelectById(id);

            return statement != null
                ? new StatementDetailQuery
                {
                    Id = statement.Id,
                    Title = statement.Title,
                    Description = statement.Description,
                    Base64Image = statement.Image,
                    Phone = statement.Phone,
                    CreatedAt = statement.CreatedAt,
                    UpdatedAt = statement.UpdatedAt
                }
                : null;
        }

        #endregion
    }
}
